export type FileProperties = {
  fileType: string;
  fileSize: string;
};

export type OffsetCoords = {
  x: number;
  y: number;
};

export type PointerCoords = {
  clientX: number;
  clientY: number;
};
